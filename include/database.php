<?php
include("constants.php");
      
class MySQLDB
{
    var $connection;         //The MySQL database connection
    var $serverconnection;  //The server connection
    var $num_members;        //Number of signed-up users
    var $help;
    var $userinfo = array();  //The array holding all user info
   
   /* Class constructor */
   function MySQLDB(){
      /* Make connection to database */
      $this->connection = mysql_connect(DB_SERVER, DB_USER, DB_PASS) or die(mysql_error());
      mysql_select_db(DB_NAME, $this->connection) or die(mysql_error());
      
      $this->serverconnection = mysql_connect("5.63.148.220", "422", "a05972b8cb") or die(mysql_error());
       mysql_select_db("422", $this->serverconnection) or die(mysql_error());
      
      $this->num_members = -1;
   }
   
   /**
    * addNewUser - Inserts the given (username, password, email)
    * info into the database. Appropriate user level is set.
    * Returns true on success, false otherwise.
    */                
   function addNewUser($subuser, $subpass, $subemail, $displayName, $steamName, $skypeName,$code,$referedBy) {
          
        $subuser = mysql_real_escape_string($subuser);
        $subpass = mysql_real_escape_string($subpass);
        $subemail = mysql_real_escape_string($subemail);
        $displayName = mysql_real_escape_string($displayName);
        $steamName = mysql_real_escape_string($steamName);
        $code = mysql_real_escape_string($code);
        $skypeName = mysql_real_escape_string($skypeName);
        $referedBy = mysql_real_escape_string($referedBy);
        
        
      $time = time();
      /* If admin sign up, give admin user level */
      if(strcasecmp($subuser, ADMIN_NAME) == 0){
         $ulevel = ADMIN_LEVEL;
         
         
      }else{
         $ulevel = USER_LEVEL;
      }
      

          $q = "INSERT INTO ".TBL_USERS."(username, password, email, display_name, steam_name, skype_name,activationcode,referedBy) VALUES ('$subuser', 'temp', '$subemail', '$displayName', '$steamName', '$skypeName','$code','$referedBy')";
      
          
          return mysql_query($q, $this->connection);
  
   }
   
   
    function resetPassword($code,$username,$email) {
        $code = mysql_real_escape_string($code);
        $username = mysql_real_escape_string($username);
        $email = mysql_real_escape_string($email);
        $userinfo = $this->getUserInfo($username);
        
        $db_resetpassword = $userinfo['resetpass'];
        $db_resetcode = $userinfo['resetcode'];
        
        if($username == $userinfo['username'] && $email == $userinfo['email'])
            if($code == $db_resetcode) {
                $result = $this->updateUserField ($username, "password", $db_resetpassword);
                
            }
            else
                return "Codes did not match";
        else 
            return "Username or Email didn't match";
        
        if($result){
            $this->updateUserField ($username, "resetpass", "");
            $this->updateUserField ($username, "resetcode", "");
            return "Your password has been reset!";
        }
        else
            return "Couldn't update user profile";
    }
    
    function addResetInfo($subuser, $subpass, $code) {
        $password = mysql_real_escape_string($subpass);
        $username = mysql_real_escape_string($subuser);
        $code = mysql_real_escape_string($code);
        
        $password = $this->nCrypt($password, $username);
        
        $this->updateUserField($username, "resetpass", $password);
        $this->updateUserField($username, "resetcode", $code);
        
        return true;
    }
   
    function addNewAlert($type,$user,$title,$content) {

        $type = mysql_real_escape_string($type);
        $user = mysql_real_escape_string($user);
        $title = mysql_real_escape_string($title);
        $content = mysql_real_escape_string($content);
        
        
        if($this->addToCache("alerts",1))  
        if($this->newLog($user,"new_alert",$type,$title,$content))
        {
            $q = "INSERT INTO opp_alerts (id, title, content, type) VALUES ((SELECT id FROM opp_user WHERE username = '$user'), '$title', '$content', '$type')";
            return mysql_query($q, $this->connection);
        }
        else
            return false;
    }
    
    function retrieveAlerts($from, $to, $order) {

        $from = mysql_real_escape_string($from);
        $to = mysql_real_escape_string($to);
        $order = mysql_real_escape_string($order);
         
        $q = "SELECT  id,title,content,type,resolved
              FROM  `opp_alerts` 
              ORDER BY  `alert_id` {$order} 
              LIMIT {$from}, {$to}";
        $result = mysql_query($q, $this->connection);
      
        return $result;
    }
   
   function ping() {
       return true;
   }
   
   function nCrypt($password,$username) {
        $userinfo = $this->getUserInfo($username);
       
        $password = mysql_real_escape_string($password);
        $passwordmd5 = md5(strtoupper($username).SEED.$password);
        $password = $passwordmd5.sha1(SEED.$password.$username);
        
        $password = $passwordmd5.sha1($password.strlen($password));
       
        
        
        $password = hash("sha256",$userinfo['date_registered'].$password);
        
        return $password;
   }
   
   function getCache() {
      $q = "SELECT * FROM opp_cache LIMIT 1";
      $result = mysql_query($q, $this->connection);
      /* Error occurred, return given name by default */
      if(!$result || (mysql_numrows($result) < 1)){
         return NULL;
      }
      /* Return result array */
      $dbarray = mysql_fetch_array($result);
      
      return $dbarray;
   }
   
   function checkCache($field) {
        $q = "SELECT * FROM opp_cache LIMIT 1";
        $result = mysql_query($q, $this->connection);
        if (!$result) echo mysql_error();

        $row = mysql_fetch_row($result);
        
        /*if($row[$field] > $session->currentCache[$field])
            return true;
        else
            return false;
         * 
         */
        return $row[$field];
   }
   
   function totalAlerts() {
        $q = "select count(*) FROM opp_alerts ";
        $result = mysql_query($q, $this->connection);
        if (!$result) echo mysql_error();

        $row = mysql_fetch_row($result);
        return $row[0];
   }
   
   function totalLogs() {
        $q = "select count(*) FROM opp_logs";
        $result = mysql_query($q, $this->connection);
        if (!$result) echo mysql_error();

        $row = mysql_fetch_row($result);
        return $row[0];
   }
   
   function totalUsers() {
        $q = "select registered_users FROM opp_cache";
        $result = mysql_query($q, $this->connection);
        if (!$result) echo mysql_error();

        $row = mysql_fetch_row($result);
        return $row[0];
   }
   
   function addToCache($field,$amount) {
       $amount = mysql_real_escape_string($amount);
 
        $q = "UPDATE opp_cache SET $field = $field+$amount WHERE cache_id = '1' LIMIT 1";
        return mysql_query($q, $this->connection);
   }
   
   function totalUnactivatedUsers() {
        $q = "select count(*) FROM opp_user WHERE activated=0";
        $result = mysql_query($q, $this->connection);
        if (!$result) echo mysql_error();

        $row = mysql_fetch_row($result);
        return $row[0];
   }
   
   function updateRank($username, $rank) {
       $username =  mysql_real_escape_string($username);
       $rank =  mysql_real_escape_string($rank);

      $q = "INSERT INTO ".TBL_PERMS."(child, parent, type) VALUES ('$username', '$rank', '1')";
      return mysql_query($q, $this->serverconnection);
       
   }
   
   function changeUserRank($user,$rank,$rank2) {
       
       global $session;
       
       $user = mysql_real_escape_string($user);
       $rank = mysql_real_escape_string($rank);
       $rank2 = mysql_real_escape_string($rank2);
       $useringame = $this->getGameRank($user);
                
       $userinfo = $this->getUserInfo($user);

       if(!$userinfo['activated'])
           $this->activate($user, $userinfo['activationcode']);
       
       if(isset($user) && isset($rank)) {
                $q = "UPDATE ".TBL_PERMS." SET parent='$rank' WHERE child='$user'";
                $res1 = mysql_query($q, $this->serverconnection);
                $this->newLog($session->username,"game_rank",$user,$useringame['parent'],$rank);
                 if(isset($rank2)) {
                      $q = "UPDATE opp_user SET level='$rank2' WHERE username='$user'";
                      $res2 = mysql_query($q, $this->connection);
                      
                      $userinfo = $this->getUserInfo($user);
                      
                    if($this->newLog($session->username, "online_rank", $user,$userinfo['level'],$rank2))
                      if($res1 && $res2)
                          return true;
                      else
                          return false;
                 }
                 
            
                return $res1;
           }
   }
   
   function checkLauncherLogin($subuser,$subpass) {
      $subuser = stripslashes($subuser);
      return $this->confirmUserPass($subuser, $subpass);
   }
   
   
   function getGameRank($user) {
       $user = mysql_real_escape_string($user);
      $q = "SELECT * FROM ".TBL_PERMS." WHERE child = '$user' LIMIT 1";
      $result = mysql_query($q, $this->serverconnection);
      /* Error occurred, return given name by default */
      if(!$result || (mysql_numrows($result) < 1)){
         return NULL;
      }
      /* Return result array */
      $dbarray = mysql_fetch_array($result);
      
      return $dbarray;
   
   }
   
   //Returns boolean
   function confirmUserPass($username,$subpassword){
      /* Add slashes if necessary (for query) */
      if(!get_magic_quotes_gpc()) {
	      $username = addslashes($username);
      }

      /* Verify that user is in database */
      $q = "SELECT password FROM ".TBL_USERS." WHERE username = '$username'";
      $result = mysql_query($q, $this->connection);
      if(!$result || (mysql_numrows($result) < 1)){
         return 1; //Indicates username failure
      }
      
        $password = $this->nCrypt($subpassword,$username);

      /* Retrieve password from result, strip slashes */
      $dbarray = mysql_fetch_array($result);
     
      $dbarray['password'] = stripslashes($dbarray['password']);
      $password = stripslashes($password);
      
      
      /* Validate that password is correct */
      if($password == $dbarray['password']){
         return 0; //Success! Username and password confirmed
      }
      else{
         return 2;
      }
   }
   
   /**
    * confirmUserID - Checks whether or not the given
    * username is in the database, if so it checks if the
    * given userid is the same userid in the database
    * for that user. If the user doesn't exist or if the
    * userids don't match up, it returns an error code
    * (1 or 2). On success it returns 0.
    */
   function confirmUserID($username, $userid){
       
       
       $username = mysql_real_escape_string($username);
       $userid = mysql_real_escape_string($userid);
      /* Add slashes if necessary (for query) */
      if(!get_magic_quotes_gpc()) {
	      $username = addslashes($username);
      }
      
      /* Verify that user is in database */
      $q = "SELECT userid FROM ".TBL_USERS." WHERE username = '$username' LIMIT 1";
      $result = mysql_query($q, $this->connection);
      if(!$result || (mysql_numrows($result) < 1)){
         return 1; //Indicates username failure
      }

    /* Retrieve userid from result, strip slashes */
      $dbarray = mysql_fetch_array($result);
      $dbarray['userid'] = stripslashes($dbarray['userid']);
      $this->help = $dbarray['userid'];

      /* Validate that userid is correct */
      if($userid == $dbarray['userid']){
         return 0; //Success! Username and userid confirmed
      }
      else{
         return 2; //Indicates userid invalid
      }
   }
   
   /**
    * usernameTaken - Returns true if the username has
    * been taken by another user, false otherwise.
   */
   function usernameTaken($username){
       $username = mysql_real_escape_string($username);
      if(!get_magic_quotes_gpc()){
         $username = addslashes($username);
      }
      $q = "SELECT username FROM ".TBL_USERS." WHERE username = '$username' LIMIT 1";
      $result = mysql_query($q, $this->connection);
      return (mysql_numrows($result) > 0);
   }
   
   function returnCode($username) {
       $username = mysql_real_escape_string($username);
       $q = "SELECT username, activationcode FROM ".TBL_USERS." WHERE username = '$username' LIMIT 1";
        
        
       $result = mysql_query($q, $this->connection);
       
       $dbarray = mysql_fetch_array($result);
      $dbarray['username'] = stripslashes($dbarray['username']);
      $dbarray['activationcode'] = stripslashes($dbarray['activationcode']);
      
      return $dbarray['activationcode'];
   }
   
   function getUserInfo($username){
       $username = mysql_real_escape_string($username);
      $q = "SELECT * FROM ".TBL_USERS." WHERE username = '$username' LIMIT 1";
      $result = mysql_query($q, $this->connection);
      /* Error occurred, return given name by default */
      if(!$result || (mysql_numrows($result) < 1)){
         return NULL;
      }
      /* Return result array */
      $dbarray = mysql_fetch_array($result);
      
      return $dbarray;
   }
   
   function getUserFromID($id){
       $id = mysql_real_escape_string($id);
      $q = "SELECT * FROM ".TBL_USERS." WHERE id='$id' LIMIT 1";
      $result = mysql_query($q, $this->connection);
      /* Error occurred, return given name by default */
      if(!$result || (mysql_numrows($result) < 1)){
         return NULL;
      }
      /* Return result array */
      $dbarray = mysql_fetch_array($result);
      
      return $dbarray;
   }
   
     function getAllUsers($from,$to,$order){
       
         $from = mysql_real_escape_string($from);
         $to = mysql_real_escape_string($to);
         $order = mysql_real_escape_string($order);
         
      $q = "SELECT * 
            FROM  `opp_user` 
            ORDER BY  `id` {$order} 
            LIMIT {$from}, {$to}";
      $result = mysql_query($q, $this->connection);
      
      return $result;
   }
   
    function getAllLogs($from,$to,$order){
       
         $from = mysql_real_escape_string($from);
         $to = mysql_real_escape_string($to);
         $order = mysql_real_escape_string($order);
         
      $q = "SELECT * 
            FROM  `opp_logs` 
            ORDER BY  `log_id` {$order} 
            LIMIT {$from}, {$to}";
      $result = mysql_query($q, $this->connection);
      
      return $result;
   }
   
    function getAllEmails(){
     
      $q = "SELECT id,username,password,email 
            FROM  `opp_user` LIMIT 10";
      $result = mysql_query($q, $this->connection);
      
      return $result;
   }
   
   function isActivated($username) {
       $username = mysql_real_escape_string($username);
       $username = strtolower($username);
       $q = "SELECT activated FROM ".TBL_USERS." WHERE username = '$username' LIMIT 1";
        
       $result = mysql_query($q, $this->connection);
       
       $dbarray = mysql_fetch_array($result);
      $dbarray['activated'] = stripslashes($dbarray['activated']);
      

      /* Validate that userid is correct */
      if($dbarray['activated'] == 1){
         return true; //Success! 
      }
      else{
         return false; 
      }
   }
   
   function activate($username,$code) {
       
       $username = strtolower($username);
       $username = mysql_real_escape_string($username);
       $code = mysql_real_escape_string($code);
       
       if(!get_magic_quotes_gpc()) {
	      $username = addslashes($username);
      }
      
      /* Verify that user is in database */
      $q = "SELECT userid FROM ".TBL_USERS." WHERE username = '$username' LIMIT 1";
      $result = mysql_query($q, $this->connection);
      if(!$result || (mysql_numrows($result) < 1)){
         return 1; //Indicates username failure
      }
       $q = "SELECT username,activated,activationcode FROM ".TBL_USERS." WHERE username = '$username' LIMIT 1";
        
        
       $result = mysql_query($q, $this->connection);
       
       $dbarray = mysql_fetch_array($result);
      $dbarray['username'] = stripslashes($dbarray['username']);
      $dbarray['activationcode'] = stripslashes($dbarray['activationcode']);
      
      /* Validate that userid is correct */
      if($dbarray['username'] == $username && $dbarray['activationcode'] == $code && $dbarray['activated'] == 0){
          $this->updateUserField($dbarray['username'], "activated", 1);
          $this->updateRank($dbarray['username'],"Registered");
        return 0; //Success! Username and code confirmed.
      }
      else{
         return 2; //Indicates code invalid.
      }
   }
   
   function query($q){
      return mysql_query($q, $this->connection);
   }
 
   function deleteUser($user) {
       
       if($this->newLog($session->username, "delete_user", $user,$user,"-")){
           if($this->addToCache("registered_users",-1))
                return mysql_query("DELETE FROM opp_user WHERE username='".$user."' AND id='1160' LIMIT 1;", $this->connection);
           else
               return false;
       }
       else
           return false;
   }
   /**
    * updateUserField - Updates a field, specified by the field
    * parameter, in the user's row of the database.
    */
   function updateUserField($username, $field, $value){
       $username = mysql_real_escape_string($username);
       $field = mysql_real_escape_string($field);
       $value = mysql_real_escape_string($value);
       
      $q = "UPDATE ".TBL_USERS." SET ".$field." = '$value' WHERE username = '$username'";
      return mysql_query($q, $this->connection);
   }
   
   function newLog($user,$action,$field_who,$previous_value,$new_value) {
       $q = "INSERT INTO opp_logs (id,action,field_who,previous_value,current_value) VALUES ((SELECT id FROM opp_user WHERE username = '$user' LIMIT 1),'$action','$field_who','$previous_value','$new_value')";
       return mysql_query($q, $this->connection);
        
   }

   function emailALL() {
      
	
    		$to = "nathan.la.harvey@gmail.com";
			$subject = "OPPCraft - TEST EMAIL";
			$body = "Good day everyone at OPPCraft.\r\n";
			$headers= "From: OPPCraft.net";

     			mail($to, $subject, $body, $headers);
    		
	

		
	/*
			$to = "bossyhar14@hotmail.co.uk";
			$subject = "OPPCraft - Server Reunion";
			$body = "Good day everyone at OPPCraft.\r\n
			
			With the new release of 1.5 the staff would like to offer an invitation for a little reunion on the server this Saturday. Come on by, if you can, around the times 6 - 10 PM (GMT). 2 -7PM(Est). Although the activities can carry on for longer after these times.
			\r\n
			With the new redstone changes I'd like to suggest a redstone related competition. We would like to get a few games running.
			\r\n
			- Classic spleef.\n
			- Build competitions.\n
			- PVP.\n
			- Lotteries.\n
			\r\n
			All with prizes to be won. A few up for grabs are: Free donator rank for the server, game of the winner's choosing, in server items and more.
			\r\n
			If you need more details feel free to reply to this message or comment on the blog post.
			\r\n
			Thank you for your time. \n
			~OPPDelta (Owner). \n
			__________________________\n
			Server IP: play.oppcraft.net // s13.hosthorde.com:25566\n
			BLOG: http://blog.oppcraft.net";
			$headers = "From: OPPCraft Minecraft" . "\r\n" ;
			$headers .= 'Reply-To: nathan.la.harvey@gmail.com' . "\r\n";
			//$headers .= "BCC: bossyhar14@hotmail.co.uk";
			$headers .= "Bcc: '".$bcc."'\r\n";

     			mail($to, $subject, $body, $headers);*/
		
    

   }
};

$database = new MySQLDB;
?>