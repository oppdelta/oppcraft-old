<?php
/* Database Constants */
define("DB_SERVER", "localhost");
define("DB_USER", "root");
define("DB_PASS", "****");
define("DB_NAME", "****");

/* //FOR REFERENCE
define("TBL_USERS", "users");
define("TBL_ACTIVE_USERS",  "active_users");
define("TBL_ACTIVE_GUESTS", "active_guests");
define("TBL_BANNED_USERS",  "banned_users");
*/

define("TBL_USERS", "op****er");
define("TBL_PERMS", "perm*******nce");
define("TBL_ALERTS", "op****rts");

/* Guest Constants */
define("GUEST_LEVEL", "0");
define("GUEST_NAME", "Guest");

define("ADMIN_LEVEL", "10");
define("ADMIN_NAME", "op****a");


define("SEED","****1");
define("PARSEKEY","1eeE112n************GVB****bifgpib****");

define("ALL_LOWERCASE","1");
define("EMAIL_WELCOME","1");

define("COOKIE_EXPIRE", 60*60*24*100);  //100 days by default
define("COOKIE_PATH", "/");  //Avaible in whole domain

?>