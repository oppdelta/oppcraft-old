<?php 
/**
 * Mailer.php
 *
 * The Mailer class is meant to simplify the task of sending
 * emails to users. Note: this email system will not work
 * if your server is not setup to send mail.
 *
 */

 
class Mailer
{
  
   function sendWelcome($user, $email, $code){
      $from = "From: OPPCraft.net";
      $subject = "Welcome to the OPPCraft community! - Here is your activation details.";
      $body = 
             "Welcome! You've just registered at OPPCraft.net "
             ."with the following username: $user.\n\n"
             ."In order to complete your registration you are required "
             ."to confirm your email address."
             ."If you ever lose or forget your password, a new "
             ."password will be generated for you and sent to this "
             ."email address, if you would like to change your "
             ."email address you can do so by going to the "
             ."account page after signing in.\n\n"
             ."If you plan on joining the Minecraft server be sure "
             ."to read the quick info page on the blog ( http://blog.oppcraft.net ).\n\n "
             ."In order for you to use the website's features and build within the server "
              ."you are required to confirm your email address. To do this, you must enter your info on the page linked below: \n\n"
              ."http://oppcraft.oppdelta.co.uk/index.php?activation=1&user=$user&code=$code"
              ."\n\n"
              ."In case the link doesn't work, please visit http://oppcraft.oppdelta.co.uk/index.php?activation=1 and "
              ."manually enter your activation code. (Located below) \n\n"
              ."Your verification code is: $code \n\n"
             ."- OPPCraft Staff";

      return mail($email,$subject,$body,$from);
   }
   
   
   function sendResetEmail($user, $email, $code){
      $from = "From: OPPCraft.net";
      $subject = "OPPCraft - Your password reset verification";
      $body = 
             "Hey there! You, or someone has requested a password reset at http://oppcraft.net "
             ."to the following username: $user.\n\n"
             ."If this was not you please ignore this email and/or report it to a member of staff "
             ."within the OPPCraft minecraft server."
             ."If you ever lose or forget your password, a new "
             ."password can be set and sent to this "
             ."email address, if you would like to change your "
             ."email address you can do so by going to the "
             ."account page after signing in.\n\n"
             ."If you plan on joining the Minecraft server be sure "
             ."to read the quick info page on the blog ( http://blog.oppcraft.net ).\n\n "
              ."Your password reset is linked below: \n\n"
              ."http://oppcraft.net/reset.php?key=$code&username=$user&email=$email"
              ."\n\n ----------------------------------------\n Minecraft server IP: play.oppcraft.net"
             ."- OPPCraft Staff";

      return mail($email,$subject,$body,$from);
   }
   
      function sendCode($user){
          global $database;
          $user = mysql_real_escape_string($user);
          
          $userinfo = $database->getUserInfo($user);
          
          $code = $userinfo['activationcode'];
          $email = $userinfo['email'];
          
        $from = "From: OPPCraft.net";
        $subject = "Account Verification Code";
        $body = "Hello, ".$user.".\n\n"
               ."You are recieving this email because you or someone has requested a new activation code. "
               ."If this wasn't you, please ignore this email. If it was you your new code is below. You can activate your account at "
                ."http://oppcraft.oppdelta.co.uk/activation.php?user=$user&code=$code"
                ." \n\n"
                ."In case the link doesn't work, please visit http://oppcraft.oppdelta.co.uk/activation.php and "
                ."manually enter your activation code.\n\n"
                ."Your verification code: $code \n\n"
               ."     -OPPCraft Staff";

      return mail($email,$subject,$body,$from);
   }
   
   /**
    * sendNewPass - Sends the newly generated password
    * to the user's email address that was specified at
    * sign-up.
    */
   function sendNewPass($user, $email, $pass){
       
       
       
      $from = "From: ".EMAIL_FROM_NAME." <".EMAIL_FROM_ADDR.">";
      $subject = "OPPTICAL - Your new password";
      $body = $user.",\n\n"
             ."We've generated a new password for you at your "
             ."request, you can use this new password with your "
             ."username to log in to the OPPTICAL Site.\n\n"
             ."Username: ".$user."\n"
             ."New Password: ".$pass."\n\n"
             ."It is recommended that you change your password "
             ."to something that is easier to remember, which "
             ."can be done by going to the Account page "
             ."after signing in.\n\n"
             ."- OPPTICAL Staff";
             
      return mail($email,$subject,$body,$from);
   }
};

/* Initialize mailer object */
$mailer = new Mailer;
 
?>