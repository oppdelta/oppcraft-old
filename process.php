<?php
include("include/session.php");

class Process
{
    function Process(){
      global $session;
    
        if(isset($_POST['sublogin'])){
         $this->procLogin();
        }
        /* User submitted registration form */
        else if(isset($_POST['subjoin'])){
           $this->procRegister();
        }
        /* User submitted forgot password form */
        else if(isset($_POST['subforgot'])){
           $this->procForgotPass();
        }
        /* User submitted edit account form */
        else if(isset($_POST['subedit'])){
           $this->procEditAccount();
        }
        /* Activate user account */
        else if(isset($_POST['subactivation'])){
           $this->procActivateAccount();
        }
        else if(isset($_POST['subnewalert'])){
         $this->procNewAlert();
        }
        else if(isset($_POST['subresetpassword'])){
         $this->procResetPassword();
        }
        else if($session->logged_in){
         $this->procLogout();
        }
        else{
          header("Location: register.php");
       }
    }
    
    /* PROCESSES BELOW */
    
    
   function procActivateAccount() {
       global $session, $form, $database;
       
       $retval = $database->activate($_POST['username'],$_POST['code']);

       if($retval == 0) {
           $database->updateUserField($user, "activated", 1);       
           header("Location: activation.php?id=1");
       }
       else
           header("Location: activation.php?id=0");

   }
    
   function procNewALert() {
       global $session, $form, $database;
   }
   
   function procLogin(){
      global $session, $form;
      /* Login attempt */
      
      
      $retval = $session->login($_POST['loginusername'],$_POST['loginpassword'],$_POST['remember']);
      
      /* Login successful */
      if($retval){
            header("Location: ".$session->referrer);
          //echo $session->referrer;
      }
      /* Login failed */
      else{
         $_SESSION['value_array'] = $_POST;
         $_SESSION['error_array'] = $form->getErrorArray();
         header("Location: ".$session->referrer);
         
      }
   }

   function procLogout(){
      global $session;
      $retval = $session->logout();
      header("Location: register.php");
   }
   function procRegister(){
      global $session, $form;
      /* Convert username to all lowercase (by option) */
      if(ALL_LOWERCASE){
         $_POST['username'] = strtolower($_POST['username']);
      }
      
      $form->setValue("username",$_POST['username']);
      $form->setValue("password",$_POST['password']);
      $form->setValue("password2",$_POST['password2']);
      $form->setValue("email",$_POST['email']);
      $form->setValue("email2",$_POST['email2']);
      $form->setValue("displayName",$_POST['displayName']);
      
      
      
      /* Registration attempt $subuser, $subpass, $subpass2, $subemail, $email2, $displayName, $steamName, $skypeName*/
      $retval = $session->register($_POST['username'], $_POST['password'], $_POST['password2'], $_POST['email'], $_POST['email2'], $_POST['displayName'], $_POST['steamName'], $_POST['skypeName'], $_POST['referedBy']);
      
      /*
      echo $_POST['username'];
      echo $_POST['password'];
      echo $_POST['password2'];
      echo $_POST['email'];
      echo $_POST['email2'];
      echo $_POST['displayName'];
      echo $_POST['steamName'];
      echo $_POST['skypeName'];*/
      
      /* Registration Successful */
      if($retval == 0){
         $_SESSION['reguname'] = $_POST['username'];
         $_SESSION['regsuccess'] = true;
         header("Location: ".$session->referrer);
      }
      /* Error found with form */
      else if($retval == 1){
         $_SESSION['value_array'] = $_POST;
         $_SESSION['error_array'] = $form->getErrorArray();
         header("Location: ".$session->referrer);
      }
      /* Registration attempt failed */
      else if($retval == 2){
         $_SESSION['reguname'] = $_POST['username'];
         $_SESSION['regsuccess'] = false;
         header("Location: ".$session->referrer);
      }
       
       
   }
   
   function procResetPassword(){
      global $session, $form;
      /* Convert username to all lowercase (by option) */
      if(ALL_LOWERCASE){
         $_POST['username'] = strtolower($_POST['username']);
      }
      
      $form->setValue("username",$_POST['username']);
      $form->setValue("password",$_POST['password']);
      $form->setValue("password2",$_POST['password2']);
      $form->setValue("email",$_POST['email']);
      
      
      /* Reset attempt */
      $retval = $session->resetPassword($_POST['username'], $_POST['password'], $_POST['password2'], $_POST['email']);

      /* Registration Successful */
      if($retval == 0){
         $_SESSION['resetuname'] = $_POST['username'];
         $_SESSION['resetsuccess'] = true;
         header("Location: ".$session->referrer);
      }
      /* Error found with form */
      else if($retval == 1){
         $_SESSION['value_array'] = $_POST;
         $_SESSION['error_array'] = $form->getErrorArray();
         header("Location: ".$session->referrer);
      }
      /* Registration attempt failed */
      else if($retval == 2){
         $_SESSION['resetuname'] = $_POST['username'];
         $_SESSION['resetsuccess'] = false;
         header("Location: ".$session->referrer);
      }
       
       
   }
};

$process = new Process;
?>
