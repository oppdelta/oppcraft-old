<?php
include("include/session.php");
$userinfo = array();
global $session;
global $database;

$session->page = "dashboard";



?>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="//buycraft.net/assets/popup/style.css" />
        <script type="text/javascript" src="//buycraft.net/assets/popup/script.js"></script>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>OPPCraft - What a Sandbox!</title>
        <link rel="stylesheet" type="text/css" href="styles/version2.css">
        <script type="text/javascript" src="js/jquery.js"></script>
       
    </head>
    <body>
                
            <table id="main_wrapper" width='100%' cellpadding='0' cellspacing='0' border='0'>
                <tr id='header'>
                    <td class='mainLeft'>
                        <div class='mainlogo'>
                            <?php include 'modules/logo.php'; ?>
                        </div>
                    </td>
                    <td class='mainRight'>
                        <div class='mainheader'>
                            <?php include 'modules/header.php'; ?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class='mainLeft'>
                        <?php include 'modules/nav.php'; ?>
                    </td>
                    <td class='mainRight'>
                        
                        <?php
                        
                        if($session->logged_in){
                            echo '<img src="http://minecraft.aggenkeech.com/body.php?d&u='.$session->username.'" />';
                        }
                        else {
                            if(isset($_SESSION['regsuccess']) || isset($_GET['activation'])) {
                                include 'modules/activation.php';
                                unset($_SESSION['regsuccess']);
                                unset($_SESSION['reguname']);
                            }
                            else {
                                
                                include 'modules/reginfo.php';
                                include 'modules/registrationform.php'; 
                            }
                        }
                         ?>
                    </td>
                </tr>
            </table>
    </body>
</html>
