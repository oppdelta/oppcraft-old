<?php
    include("../include/session.php");
    include("../include/functions.php");
    $userinfo = array();
    global $session;
    global $database;
    
  
?>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>OPPCraft Staff Portal - Dashboard</title>
        <link rel="stylesheet" type="text/css" href="css/maintheme.css">
        
        <!--[if gte IE 9]>
        <style type="text/css">
          .gradient {
             filter: none;
          }
        </style>
      <![endif]-->
          <!--[if lt IE 9]>
  <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <script type="text/javascript">
            document.createElement("nav");
            document.createElement("header");
            document.createElement("footer");
            document.createElement("section");
            document.createElement("aside");
            document.createElement("article");
  </script>
  <![endif]-->
        
        
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>

<script type="text/javascript">// <![CDATA[
/*$(document).ready(
function() {
$.ajaxSetup({ cache: false }); // This part addresses an IE bug.  without it, IE will only load the first number and will never refresh
setInterval(function() {
    $('#serverinfo').load('plugins/serverinfo.php');
}, 25000); // the "3000" here refers to the time to refresh the div.  it is in milliseconds.
});*/
// ]]>

$(document).ready(function() {
$.ajaxSetup({ cache: false }); // This part addresses an IE bug.  without it, IE will only load the first number and will never refresh
setInterval(function() {
    $('#alertsbox').load('plugins/alerts.php');
}, 5000); // the "5000" here refers to the time to refresh the div.  it is in milliseconds.
});
$(document).ready(function() {  

        //when button is clicked  
        $('#submitnewalert').click(function(){  
            //run the character number check  
                submit_alert(); 
        });  
  
        $('#result').text( $('#ranks').find('option:selected').text());
        $('#ranks').change(function(){    
        $('#result').text( $(this).find('option:selected').text());
});

  
  });  
  
  function submit_alert(){  
  
        //get the username  
        var alertContent = $('#alertContent').val();  
        var alertTitle = $('#alertTitle').val();
        var alertType = $('#alertType').val();
  
        //use ajax to run the check  
        $.post("createalert.php", { alertType: alertType, alertTitle: alertTitle, alertContent: alertContent },  
            function(result){  
                //if the result is 1  
                if(result == 1){  
                    $('#box').html('Alert added!');
                }else{  
                    $('#box').html('There was an unknown error...');  
                }  
        });  
  
}  

</script>
    </head>
    <body>
        <nav>
        <?php
            include 'plugins/nav.php';
        ?>
        </nav>
        <section id='contentwrapper'>
            <?php if(!$session->logged_in && $session->username == "Guest") { 
                header("Location: login.php");
               
            } else if($session->logged_in && $session->userlevel >=7){ ?>
                <header id='mainheader'>
                <?php
                    include 'plugins/topnav.php';
                                    ?>
                </header> 
            <article id="innerwrapperLeft">
                <?php
                    include 'plugins/serverinfo.php'; 
                    include 'plugins/latestuserstable.php';
                ?>
            </article>
            <article id="innerwrapperRight">
                
                
                
                <?php
                    include 'plugins/alerts.php'; 
                    include_once 'plugins/newalertform.php';
                ?>
                
                
                
                <h3>Latest YouTube Video</h3>
                <iframe width="800px" height="480" src="//www.youtube.com/embed/xlmixiELyEo?rel=0" frameborder="0" allowfullscreen></iframe>
            </article>
            
            <?php } else echo "You can't access this page!"; ?>
        </section>
        <div style="clear:both;">HEY</div>
    </body>
</html>
