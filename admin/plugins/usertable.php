<?php 
$admin_settings = '<td>
            <img class="settings" src="images/Close.png" title="Delete User From Database" alt="Delete User From Database" />
            <img class="settings" src="images/Up.png" title="Promote User" alt="Promote User" />
            <img class="settings" src="images/Refresh.png" title="Resend Users Activation Code" alt="Resend Users Activation Code" />
            <img class="settings" src="images/Edit.png" title="Edit User" alt="Edit user" />
        </td>'; 

$mod_settings = '<td>
            <img class="settings" src="images/Up.png" title="Promote User" alt="Promote User" />
            <img class="settings" src="images/Refresh.png" title="Resend Users Activation Code" alt="Resend Users Activation Code" />
            <img class="settings" src="images/Edit.png" title="Edit User" alt="Edit user" />
        </td>';
if(!isset($_GET['from']))
    $currentamount = 0;
else
    $currentamount = mysql_real_escape_string($_GET['from']);
?>

<h3>Users</h3>
<p><a href="users.php" title="First 50 Users"><<</a> | 
    <a href="<?php echo "users.php?from=".$from=$currentamount-50; ?>" title="Previous 50 Users"><</a> | 
    <a href="<?php echo "users.php?from=".$from=$currentamount+50; ?>" title="Next 50 Users">></a> | 
    <a href="<?php echo "users.php?from=".$from=$database->totalUsers()-50; ?>" title="Last 50 Users">>></a>
</p>
<table class="usersTable" cellpadding="0" cellspacing="1" width="1200px">
    <th width="160px" style="background-color: #b3b3b3;">Minecraft Name</th>
    <th width="160px" style="background-color: #89ae9e;">OPPCraft Name</th>
    <th width="95px" style="background-color: #b3b3b3;">Activated</th>
    <?php if($session->userlevel > 9)
        echo '<th width="260px" style="background-color: #89ae9e;">Email</th>' ?>
    <th width="95px" style="background-color: #b3b3b3;">Act. Code</th>
    <th width="150px" style="background-color: #89ae9e;">Steam</th>
    <th width="150px" style="background-color: #b3b3b3;">Skype</th>
    <th width="150px" style="background-color: #89ae9e;">MC Rank</th>
    <th width="120px" style="background-color: #b3b3b3;">Web Rank</th>
    <th width="100px" style="background-color: #89ae9e;">Registered</th>
    <th width="160px" style="background-color: #b3b3b3;">Referrer</th>
    <th width="160px" style="background-color: #89ae9e;">Settings</th>
    <?php
    
    $result = $database->getAllUsers($currentamount,50,"ASC");
    
    
    while(($row = mysql_fetch_array($result)))
    {
        $mcrank = $database->getGameRank($row['username']);
        echo "<tr>";
        echo "<td>".$row['username']."</td>";
        echo "<td class='green'>".$row['display_name']."</td>";
        echo "<td>".toHex($row['activated'])."</td>";
        if($session->userlevel > 9)
            echo "<td class='green'>".$row['email']."</td>";
        echo "<td>".$row['activationcode']."</td>";
        echo "<td class='green'>".$row['steam_name']."</td>";
        echo "<td>".$row['skype_name']."</td>";
        echo "<td class='green'>".$mcrank['parent']."</td>";
        echo "<td>".$row['level']."</td>";
        echo "<td class='green'>".$row['date_registered']."</td>";
        echo "<td>".$row['referedBy']."</td>";
        echo staffSettings($row['username']);
        echo "</tr>";
    }
    
    ?>
</table>

<p><a href="users.php" title="First 50 Users"><<</a> | 
    <a href="<?php echo "users.php?from=".$from=$currentamount-50; ?>" title="Previous 50 Users"><</a> | 
    <a href="<?php echo "users.php?from=".$from=$currentamount+50; ?>" title="Next 50 Users">></a> | 
    <a href="<?php echo "users.php?from=".$from=$database->totalUsers()-50; ?>" title="Last 50 Users">>></a>
</p>