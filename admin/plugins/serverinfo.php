<span id="serverinfo">
<h3>Server Info</h3>

<ul id='SI_left'>
    <li>Server status: <?php 
    
    if(!isset($database))
    {
    include("../../include/database.php"); 
        global $database;
    }
    
    function pingAddress($url, $port) { 
    $socket = stream_socket_client(sprintf('tcp://%s:%u', $url, $port), $errno, $errstr, 2);

    if ($socket === false) {
            return false;
    }

    fwrite($socket, "\xfe\x01");
    $data = fread($socket, 256); 
    fclose($socket);

    // Is this a disconnect with the ping?
    if ($data == false AND substr($data, 0, 1) != "\xFF") { 
            return false;
    }

    $data = substr($data, 9);
    $data = mb_convert_encoding($data, 'auto', 'UCS-2');
    $data = explode("\x00", $data);
    //var_dump($data);
    return array(
        'protocol_version'  => $data[0],
        'mc_version'        => $data[1],
        'motd'              => $data[2],
        'players'           => $data[3],
        'max_players'       => $data[4],
    );
    
}
    $result = false;
    $result = pingAddress("s13.hosthorde.com","25566"); 
    if($result === false) {
        echo "<font style='color: red; font-weight: bold;'>OFFLINE </font></li>
            <li>Users online: -/-</li>";
    } 
    else {
        echo "<font style='color: green; font-weight: bold;'>ONLINE</font> </li>
    <li>Users online: {$result['players']}/{$result['max_players']}</li>

";} ?>
    <li>Web guests: #</li></ul>
<ul id='SI_right'>
    <li>Registered users: <?php echo $database->totalUsers();?></li>
    <li>Unactivated users: #</li>
    <li>OML connections: #</li>
</ul>
</span>