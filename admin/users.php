<?php
    include("../include/session.php");
    include("../include/functions.php");
    $userinfo = array();
    global $session;
    
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>OPPCraft Staff Portal - User Database</title>
        <link rel="stylesheet" type="text/css" href="css/maintheme.css">
    </head>
    <body>
        <nav>
        <?php
            include 'plugins/nav.php';
        ?>
        </nav>
        
        <section id='contentwrapper'>
            <?php if(!$session->logged_in && $session->username == "Guest") { 
                header("Location: login.php");
               
            } else if($session->userlevel >=7){ ?>
                <header id='mainheader'>
                <?php
                    include 'plugins/topnav.php';
                ?>
                </header> 
            <article id="innerwrapper">
                
                <h3>Search</h3> 
                <form name="search" method="POST" action="users.php">
                Search for: 
                <Select name="field">
                <Option value="username">Username</option>
                <Option value="activationcode">Code</option>
                <?php if($session->userlevel > 9) 
                echo '<Option value="email">Email</option>'; ?>
                </Select>
                <input type="text" name="find" />
                <input type="hidden" name="searching" value="1" />
                <input type="submit" name="search" value="Search" />
                </form>
                
                
                
                <?php if(!isset($_POST['searching'])) 
                        include 'plugins/usertable.php'; 
                else {
                    
                    if(isset($_POST['field']))
                        $field = mysql_real_escape_string ($_POST['field']);
                    if(isset($_POST['find']))
                        $find = mysql_real_escape_string ($_POST['find']);
                    
                     
                    //If they did not enter a search term we give them an error 
                    if ($find == "") 
                    { 
                        echo "<p>You forgot to enter a search term</p>"; 
                        include 'plugins/usertable.php';
                        exit; 
                    } 
                    else {
                        echo "<h2>Results</h2>";
                        
                    ?>
                    <table class="latestUsersTable" cellpadding="0" cellspacing="1" width="760px">
                        <th width="200px" style="background-color: #b3b3b3;">Minecraft Name</th>
                        <th width="90px"style="background-color: #89ae9e;">Activated</th>
                        <?php if($session->userlevel > 9) 
                        echo '<th width="260px" style="background-color: #b3b3b3;">Email</th>'; ?>
                        <th width="90px" style="background-color: #89ae9e;">Act. Code</th>
                        <th width="90px" style="background-color: #b3b3b3;">Web Rank</th>
                        <th width="90px" style="background-color: #89ae9e;">MC Rank</th>
                        <th width="120px" style="background-color: #b3b3b3;">Registered</th>
                        <th width="160px" style="background-color: #89ae9e;">Settings</th>

                        <?php

                        $result = mysql_query("SELECT username,email,activationcode,activated,level,date_registered FROM opp_user WHERE upper($field) LIKE'%$find%' LIMIT 1",$database->connection); 

                        while(($row = mysql_fetch_array($result)))
                        {
                            $mcrank = $database->getGameRank($row['username']);
                            echo "<tr>";
                            echo "<td>".$row['username']."</td>";
                            echo "<td class='green'>".toHex($row['activated'])."</td>";
                            if($session->userlevel > 9)
                            echo "<td>".$row['email']."</td>";
                            echo "<td class='green'>".$row['activationcode']."</td>";
                            echo "<td>".$row['level']."</td>";
                            echo "<td class='green'>".$mcrank['parent']."</td>";
                            echo "<td>".$row['date_registered']."</td>";
                            echo staffSettings($row['username']);
                            echo "</tr>";
                        }

                        ?>


                    </table>
                        <?php
                      
                        include 'plugins/usertable.php';
                    }
                }
                ?>
            </article>
            <?php } else echo "You can't access this page!"; ?>
        </section>
        <div style="clear:both;">HEY</div>
    </body>
</html>
