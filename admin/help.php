<?php
    include("../include/session.php");
    include("../include/functions.php");
    $userinfo = array();
    global $session;
    global $database;
?>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>OPPCraft Staff Portal - Help</title>
        <link rel="stylesheet" type="text/css" href="css/maintheme.css">
        
        <!--[if gte IE 9]>
        <style type="text/css">
          .gradient {
             filter: none;
          }
        </style>
      <![endif]-->
          <!--[if lt IE 9]>
  <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <script type="text/javascript">
            document.createElement("nav");
            document.createElement("header");
            document.createElement("footer");
            document.createElement("section");
            document.createElement("aside");
            document.createElement("article");
  </script>
  <![endif]-->
        
        
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>

    </head>
    <body>
        <nav>
        <?php
            include 'plugins/nav.php';
        ?>
        </nav>
        <section id='contentwrapper'>
            <?php if(!$session->logged_in && $session->username == "Guest") { 
                header("Location: login.php");
               
            } else if($session->logged_in && $session->userlevel >=7){ ?>
                <header id='mainheader'>
                <?php
                    include 'plugins/topnav.php';
                ?>
                </header> 
            <article id="innerwrapperLeft">
                <h3>Core Protect / Block Logging Help</h3>
                <h4>Link here: <a href='#'/>http://dev.bukkit.org/bukkit-plugins/coreprotect/</a></h4>
                <iframe width="810" height="470" src="//www.youtube.com/embed/uAgS5cj5J9Q?rel=0" frameborder="0" allowfullscreen></iframe>
            
                <h3>Ban Management (How To around 12:20)</h3>
                <h4>Link here: <a href='#'/>http://dev.bukkit.org/bukkit-plugins/ban-management/</a></h4>
                <iframe width="810" height="470" src="//www.youtube.com/embed/uesIWLZrto8?rel=0" frameborder="0" allowfullscreen></iframe>
            </article>
            <article id="innerwrapperRight">
                <h3>World-Edit</h3>
                <h4>Link here: <a href='#'/>http://dev.bukkit.org/bukkit-plugins/worldedit/</a></h4>
                <iframe width="810" height="470" src="//www.youtube.com/embed/1-sYSdwM-Lc?rel=0" frameborder="0" allowfullscreen></iframe>
            </article>
            
            <?php } else echo "You can't access this page!"; ?>
        </section>
        <div style="clear:both;">HEY</div>
    </body>
</html>
