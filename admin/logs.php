<?php
    include("../include/session.php");
    $userinfo = array();
    global $session;
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>OPPCraft Staff Portal - Logs</title>
        <link rel="stylesheet" type="text/css" href="css/maintheme.css">
    </head>
    <body>
        <nav>
        <?php
            include 'plugins/nav.php';
        ?>
        </nav>
        <section id='contentwrapper'>
            <?php if(!$session->logged_in && $session->username == "Guest") { 
                header("Location: login.php");
               
            } else if($session->userlevel >=7){ ?>
                <header id='mainheader'>
                <?php
                    include 'plugins/topnav.php';
                ?>
                </header> 
            <article id="innerwrapper">
                <?php
                    include 'plugins/logstable.php';
                ?>
            </article>
            <?php } else echo "You can't access this page!"; ?>
        </section>
    </body>
</html>
