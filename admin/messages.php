<?php
    include("../include/session.php");
    include("../include/functions.php");
    $userinfo = array();
    global $session;
    global $database;
?>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>OPPCraft Staff Portal - Dashboard</title>
        <link rel="stylesheet" type="text/css" href="css/maintheme.css">
        
        <!--[if gte IE 9]>
        <style type="text/css">
          .gradient {
             filter: none;
          }
        </style>
      <![endif]-->
          <!--[if lt IE 9]>
  <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <script type="text/javascript">
            document.createElement("nav");
            document.createElement("header");
            document.createElement("footer");
            document.createElement("section");
            document.createElement("aside");
            document.createElement("article");
  </script>
  <![endif]-->
        
        
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>

    </head>
    <body>
        <nav>
        <?php
            include 'plugins/nav.php';
        ?>
        </nav>
        <section id='contentwrapper'>
            <?php if(!$session->logged_in && $session->username == "Guest") { 
                header("Location: login.php");
               
            } else if($session->userlevel >=7){ ?>
                <header id='mainheader'>
                <?php
                    include 'plugins/topnav.php';
                ?>
                </header> 
            <article id="innerwrapperLeft">
                <?php
                    include 'plugins/serverinfo.php'; 
                    include 'plugins/latestuserstable.php';
                ?>
            </article>
            <article id="innerwrapperRight">
                
                
                
                <?php
                    include 'plugins/alerts.php'; 
                    include_once 'plugins/newalertform.php';
                ?>
                
                
                
                <h3>Latest YouTube Video</h3>
                <iframe width="800px" height="480" src="//www.youtube.com/embed/xlmixiELyEo?rel=0" frameborder="0" allowfullscreen></iframe>
            </article>
            
            <?php } else echo "You can't access this page!"; ?>
        </section>
        <div style="clear:both;">HEY</div>
    </body>
</html>
