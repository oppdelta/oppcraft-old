<?php
    include("../include/session.php");
    include("../include/functions.php");
    $userinfo = array();
    global $session;
    
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>OPPCraft Staff Portal - Login</title>
        <link rel="stylesheet" type="text/css" href="css/maintheme.css">
    </head>
    <body>
        <nav>
        <?php
            include 'plugins/nav.php';
        ?>
        </nav>
        
        <section id='contentwrapper'>
            <?php if(!$session->logged_in && $session->username == "Guest") { 
                ?>
            
            <header id='mainheader'>
                <?php
                    include 'plugins/topnav.php';
                ?>
            </header> 
            <article id="innerwrapper">
                <form id="loginformmain" name="form_Login" action="../process.php" method="POST">
                 <h3 style="text-align: left;">Login</h3>
                    <label class="loginLabel" for="display">Minecraft Username</label>
                    <input class="textfield" name="loginusername" type="text" length="25" />
                    <br />
                    <label class="loginLabel" for="password">Password</label>
                    <input class="textfield" name="loginpassword" type="password" length="25" />
                    <br /> <center>
                    <input type="checkbox" name="remember" checked="1" value="Yes" />
                    <font style="font-size: 0.9em">Remember me</font>
                    <input type="hidden" name="sublogin" value="1">
                    <input class="button" type="submit" value="Login"></center>
                </form>
                <?php echo $form->error("loginpass"); ?>
                <?php echo $form->error("loginuser"); ?>
            </article>
            
             </section>
        <div style="clear:both;">HEY</div>
    </body>
</html>
            
            <?php
            } else {header("Location: index.php");}?>
       