<div class="contentBoxWide">
    <img class="alert" src="images/icons/Alert.png" alt="alert box" /><h1>Log-in to your account</h1>
    
    
    <div class='innerwrapper'>
        <form id="loginformmain" name="form_Login" action="process.php" method="POST">
         
         <ul>
            <li>
               <label class='small'>username</label>
               <input name="loginusername" type="text" length="27" />
            </li>
            <li>
               <label class='small'>password</label>
               <input name="loginpassword" type="password" length="27" />
            </li>
            <li>

               <font style="font-size: 0.6em;">Remember me</font>
               <input type="checkbox" name="remember" checked="1" value="Yes" />
               <input type="hidden" name="sublogin" value="1">
               <input class="greenButton" type="submit" value="Login"><br/>
               <font style="font-size: 0.6em;"><a href='reset.php'>Forgot your password?</a></font>

            </li>
         </ul>
        </form>
    </div>
    
        
        
        <?php echo $form->error("loginpass"); ?>
        <?php echo $form->error("loginuser"); ?>
</div>