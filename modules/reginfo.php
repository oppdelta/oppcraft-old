<div class="contentBoxWide">
    <img class="alert" src="images/icons/Alert.png" alt="alert box" /><h1>Introduction</h1> 
    <p>
       In order to build on the server you are, unfortunately, required to register. This is free and quick, either fill out the 
quick registration form below this box or click REGISTER in the navigation bar. This is to stop lazy griefers and 
spammers. If you have trouble contact someone in game, they should help. Thank you.
    </p>
</div>
