<?php if($session->logged_in) {?>

<ul class="mainMenu">

    <a href="index.php"><li><img src="images/icons/Home.png" />Dashboard</li></a>

    <a href="users.php"><li><img src="images/icons/User.png" />My Account</li></a>

    <a href="http://forum.oppcraft.net" target="_blank"><li><img src="images/icons/Chat.png" />Forum</li></a>

    <a href="http://blog.oppcraft.net/p/getting-started.html" target="_blank"><li><img src="images/icons/Info.png" />Help & Info</li></a>

    <a href="http://opptical.buycraft.net/category/89306"><li><img src="images/icons/Tag.png" />Donations</li></a>

    <a href="http://blog.oppcraft.net"><li><img src="images/icons/RSS.png" />Blog</li></a>

    <a href="events.php"><li><img src="images/icons/Calender.png" />Events</li></a>

    <a href="gallery.php"><li><img src="images/icons/Pictures.png" />Gallery</li></a>

    <?php if($session->userlevel > 7) echo 

        '<a href="admin/index.php"><li><img src="images/icons/Tools.png" />Staff Panel</li></a>'; ?>

    <a href="logout.php"><li><img src="images/icons/Lock.png" />Logout</li></a>

</ul>

<?php

    }

    else {

?>

<ul class="mainMenu">

    <a href="index.php"><li class='registerLi'><img src="images/icons/Key.png" />REGISTER</li></a>

    <a href="login.php"><li class='loginLi'><img src="images/icons/Lock.png" />Login</li></a>

    <a href="http://forum.oppcraft.net" target="_blank"><li><img src="images/icons/Chat.png" />Forum</li></a>

    <a href="http://blog.oppcraft.net/p/getting-started.html" target="_blank"><li><img src="images/icons/Info.png" />Help & Info</li></a>

    <a href="http://opptical.buycraft.net/checkout/packages?popup=true&action=add&package=426904" class="buycraft-popup"><li><img src="images/icons/Tag.png" />Donate</li></a>

    <a href="http://blog.oppcraft.net"><li><img src="images/icons/RSS.png" />Blog</li></a>

    <a href="gallery.php"><li><img src="images/icons/Pictures.png" />Gallery</li></a>

</ul>

<?php } ?>



