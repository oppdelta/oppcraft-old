<?php
    if(isset($_GET['user']))
        $user = $_GET['user'];
    else
        $user = NULL;
    if(isset($_GET['code']))
        $code = $_GET['code'];
    else
        $code = NULL;
?>

<div class="contentBoxWide">
    <img class="alert" src="images/icons/Alert.png" alt="alert box" /><h1>OPPCraft Activation</h1> 
    <h5>Thank you for registering!</h5>
    <p>
        Once you have activated your account it can take up-to 5 minutes for your rank to update in-game. If it doesn’t update after 5 minutes, re-log or contact a
        member of staff. They will have a [C],[M] or [A] before their names.
    </p>
    <form id="registerForm" name="registerForm" action="process.php" method="POST">
          <fieldset>
            <legend>Activation details</legend>
            <ul class="registerFormList">
                <li><label>minecraft username</label><input title="Your Minecraft Username" type="text" name="username" size="40" value="<?php echo $user; ?>"/></li><div><?php echo $form->error("user"); ?></div>
                <li><label>activation code</label><input title="Your account's activation code" type="text" name="code" size="10" value="<?php echo $code; ?>"/></li><div><?php echo $form->error("code"); ?></div>
            </ul>
          </fieldset>
          <span class="fullWidthRight"> 
           <input class="greenButton" type="reset" name="submit" value="clear form" /> 
           <input class="greenButton" type="submit" name="submit" value="activate account" /> 
          </span>
            <input type='hidden' name='subactivation' value='1' />
    </form>
</div>