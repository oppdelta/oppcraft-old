<div class="contentBoxWide">
    <img class="alert" src="images/icons/Alert.png" alt="alert box" /><h1>OPPCraft Registration</h1> 
    <p>
        Once you have registered and activated your account it can take up-to 5 minutes for your rank to update in-game. If it doesn’t update after 5 minutes, re-log or contact a
        member of staff. They will have a [C],[M] or [A] before their names.
        You can create your own password (This is your OPPCraft account password).
    </p>
    <form id="registerForm" name="registerForm" action="process.php" method="POST">
          <fieldset>
            <legend>Required Details</legend>
            <ul class="registerFormList">
                <li><label>minecraft username</label><input value="<?php echo $form->value("username"); ?>" title="Your Minecraft Username" type="text" name="username" size="46"/><?php echo $form->error("username"); ?></li>
                <li><label>oppcraft password</label><input value="<?php echo $form->value("password"); ?>" title="A password for your OPPCraft account" type="password" name="password" size="46"/><?php echo $form->error("password"); ?></li>
                <li><label>confirm password</label><input value="<?php echo $form->value("password2"); ?>" title="Password validation" type="password" name="password2" size="46"/><?php echo $form->error("password2"); ?></li>
                <li><label>display name</label><input value="<?php echo $form->value("displayName"); ?>" title="Your Display Name, this is your public name" type="text" name="displayName" size="46"/><?php echo $form->error("displayName"); ?></li>
                <li><label>email address</label><input value="<?php echo $form->value("email"); ?>" title="Your email, so we can send your activation code" type="text" name="email" size="46"/><?php echo $form->error("email"); ?></li>
                <li><label>confirm email</label><input value="<?php echo $form->value("email2"); ?>" title="Confirm email" type="text" name="email2" size="46"/><?php echo $form->error("email2"); ?></li>
            </ul>
          </fieldset>
        
          <fieldset>
            <legend>Optional personal details</legend>
            <ul class="registerFormList">
                <li><label>steam</label><input title="Your Steam username" type="text" name="steamName" size="46"/></li>
                <li><label>skype</label><input title="Your Skype username" type="text" name="skypeName" size="46"/></li>
                <li><label>twitter</label><input title="Your Twitter account" type="text" name="twitterName" size="46"/></li>
                <li><label>website</label><input title="Your website" type="text" name="website" size="46"/></li>
                <li><label>youtube</label><input title="Your Youtube username" type="text" name="youtubeName" size="46"/></li>
            </ul>
          </fieldset>
          <span class="fullWidthRight"> 
           <input class="greenButton" type="reset" name="submit" value="clear form" /> 
           <input class="greenButton" type="submit" name="submit" value="register account" /> 
          </span>
            <input type='hidden' name='subjoin' value='1' />
    </form>
</div>

