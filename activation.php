<?
    //include("include/session.php");
?>
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>OPPCraft - Activation Page!</title>
  <meta name="description" content="OPPTICAL Minecraft server. News, mods and fun!">
  <meta name="author" content="OPPDesigns">
  
  <!--[if lt IE 9]>
  <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <script type="text/javascript">
            document.createElement("nav");
            document.createElement("header");
            document.createElement("footer");
            document.createElement("section");
            document.createElement("aside");
            document.createElement("article");
  </script>
  <![endif]-->
</head>
<body>
   
    <section class="container">  
        <?php  
        global $database;

        $user = htmlentities($_GET['user']);
        $code = htmlentities($_GET['code']);
        
            if(isset($_GET['id'])) {
                if($_GET['id'] == 0) {
                    echo '<header class="newsHeader">Account Activation</header>
                    <p class="newsContent">
                       We\'re sorry, but something went wrong in your activation. Either the username/activation code was wrong.  Or you\'re already activated. Please try again. <br /> 
                    </p>
                    
                    <form action="process.php" method="POST">
                        <table>
                            
                            <tr><td>Username:</td><td><input type="text" name="username" maxlength="30" value="'.$user.'"></td></tr>
                            <tr><td>Code:</td><td><input type="text" size="10" name="code" maxlength="10" value="'.$code.'"></td></tr>
                            <tr><td><input type="hidden" name="subactivation" value="1"></td></tr>                            
                            <tr><td><input type="submit" value="Activate Account"></td></tr>
                        </table>
                    </form>
                    </header>';
                }
                
                    else if($_GET['id'] == 1)
              echo '
                  
                  <header class="newsHeader">Account Activation</header>
                    <p class="newsContent">
                       You have successfully activated your account, you may now login and play! <br />It may take up to 5 minutes for your username to update in game. <br /> Thank you.
                    </p>
                    </header>
                <br />';
             
            } else
            {
                echo '<header class="newsHeader">Account Activation</header>
                    <p class="newsContent">
                       Hello, in order to fully complete your registration you are required to confirm your email address. An
                       activation code has been sent to the email you registered with. Click the link in the email or manually enter your activation code below. Thank you.</p>
                    
                    <form action="process.php" method="POST">
                        <table>
                            
                            <tr><td>Username:</td><td><input type="text" name="username" maxlength="30" value="'.$user.'"></td></tr>
                            <tr><td>Code:</td><td><input type="text" size="10" name="code" maxlength="10" value="'.$code.'"></td></tr>
                            <tr><td><input type="hidden" name="subactivation" value="1"></td></tr>                            
                            <tr><td><input type="submit" value="Activate Account"></td></tr>
                        </table>
                    </form>
                    </header>';
            }
        ?>
        
    </div>        
        
    </section>
    
</body>
</html>
