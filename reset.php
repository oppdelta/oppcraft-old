<?php
include("include/session.php");
$userinfo = array();
global $session;
global $database;

$session->page = "dashboard";

?>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="//buycraft.net/assets/popup/style.css" />
        <script type="text/javascript" src="//buycraft.net/assets/popup/script.js"></script>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>OPPCraft - Password reset page!</title>
        <link rel="stylesheet" type="text/css" href="styles/version2.css">
        <script type="text/javascript" src="js/jquery.js"></script>
       
    </head>
    <body>
                
            <table id="main_wrapper" width='100%' cellpadding='0' cellspacing='0' border='0'>
                <tr id='header'>
                    <td class='mainLeft'>
                        <div class='mainlogo'>
                            <?php include 'modules/logo.php'; ?>
                        </div>
                    </td>
                    <td class='mainRight'>
                        <div class='mainheader'>
                            <?php include 'modules/header.php'; ?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class='mainLeft'>
                        <?php include 'modules/nav.php'; ?>
                    </td>
                    <td class='mainRight'> <?php
                    if(isset($_SESSION['resetsuccess'])){
                        echo "Thank you ".$_SESSION['resetuname'].". An email containing information on how to reset your password has been sent to your registered email account.";
                        unset($_SESSION['resetsuccess']);
                        }
                        else if(isset($_GET['key'])) {
                            $result = $database->resetPassword($_GET['key'],$_GET['username'],$_GET['email']);
                            echo $result."<hr />";
                        }
                        else ?>
                                        <div class="contentBoxWide">
                                            <img class="alert" src="images/icons/Alert.png" alt="alert box" /><h1>Password Reset</h1> 
                                            <p>
                                        <form action="process.php" method="POST">
                                            <table>
                                                <tr><td>Username:</td><td><input type="text" name="username" maxlength="30" size="25" value="<?php echo $form->value("username"); ?>"></td></tr>
                                                <tr><td>Email:</td><td><input type="text" size="25" name="email" maxlength="50" value="<?php echo $form->value("email"); ?>"></td></tr>
                                                <tr><td>New Password:</td><td><input type="password" size="25" name="password" maxlength="50"></td></tr>
                                                <tr><td>Confirm Password:</td><td><input type="password" size="25" name="password2" maxlength="50"></td></tr>
                                                <tr><td><input type="hidden" name="subresetpassword" value="1"></td></tr>                            
                                                <tr><td><input type="submit" value="Reset Password"></td></tr>
                                            </table>
                                        </form>
                                        </header>
                                        </p></div>
                    <?php
                        
                        echo $form->error('username');
                        echo $form->error('email');
                        echo $form->error('password');
                        echo $form->error('password2');
                        ?>
                    </td>
                </tr>
            </table>
    </body>
</html>
